#!/bin/bash

minikube config set cpus 4
minikube config set memory 8192
minikube config set disk-size 50g

minikube delete
rm ./terraform/terraform.tfstate*

minikube start
minikube addons enable ingress

cd terraform
./terraform.sh

minikube dashboard
